<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
        "id"=> $this->id,
        'name'=>$this->name,
        'image_url'=>$this->image_url,
        "description_group"=>$this->description_group,
        "categories_id"=>$this->categories_id,
        "types_id"=>$this->types_id
        ];
    }
}
