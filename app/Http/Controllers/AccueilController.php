<?php

namespace App\Http\Controllers;
use App\Group;

use Illuminate\Http\Request;

class AccueilController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        return view('group',['groups'=>$groups]);
    }
}
