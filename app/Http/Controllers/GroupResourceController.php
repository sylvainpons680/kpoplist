<?php

namespace App\Http\Controllers;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Resources\GroupResource;

class GroupResourceController extends Controller
{
    public function index()
    {
        $groups = Group::get();
       return GroupResource::collection($groups);

    }
}
