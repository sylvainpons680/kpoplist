<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagesMembers extends Model
{
    protected $fillable = [
        'images_list'
    ];
    public $timestamps = false;

    public function groups()
    {

    return $this->belongsTo('App\Member');
    
    }
}
