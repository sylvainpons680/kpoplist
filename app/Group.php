<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable=[
        'name',
        'image_url',
        "description_group",
        "categories_id",
        "types_id"
    ];
    public $timestamps = false;
    
    public function categories()
    {
        return $this->belongsTo('App\Category');
    }
    public function types()
    {

    return $this->belongsTo('App\Type');

    }

    
}
