<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupsImages extends Model
{
    protected $fillable =[
        'images_groupe'
    ];
    public $timestamps = false;
    
    public function groups()
    {

    return $this->belongsTo('App\Group');
    
    }
}
