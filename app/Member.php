<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable =[
        "name",
        "descritpion_member",
        'images_member'
    ];

    public function groups()
    {

    return $this->belongsTo('App\Group');
    
    }
    public $timestamps = false;
}
