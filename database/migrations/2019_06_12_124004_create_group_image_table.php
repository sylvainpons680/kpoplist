<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_images', function (Blueprint $table) {
            $table
            ->bigIncrements('id');

            $table
            ->bigInteger('groups_id')
            ->unsigned();

            $table
            ->foreign('groups_id')
            ->references('id')->on('groups');

            $table
            ->string('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_images');
    }
}
