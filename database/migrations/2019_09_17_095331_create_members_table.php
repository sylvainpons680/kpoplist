<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table
            ->bigIncrements('id');

            $table
            ->string('name')
            ->nullable();

            $table->bigInteger('groups_id')
            ->unsigned()->nullable();

            $table
            ->string('images_member')
            ->nullable();

            $table
            ->text('description_member')
            ->nullable();

            $table
            ->foreign('groups_id')
            ->references('id')->on('groups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
