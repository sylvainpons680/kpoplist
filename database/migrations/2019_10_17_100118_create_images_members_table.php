<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_members', function (Blueprint $table) {
            $table
            ->bigIncrements('id');
            $table
            ->bigInteger('members_id')
            ->unsigned();

            $table
            ->foreign('members_id')
            ->references('id')->on('members');

            $table->string('images_list');

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_members');
    }
}
