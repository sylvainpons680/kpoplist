<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
            
            $table->bigInteger('categories_id')
            ->unsigned()->nullable();
            
            $table->bigInteger('types_id')
            ->unsigned()->nullable();

            $table->foreign('categories_id')
            ->references('id')->on('categories');

            $table->foreign('types_id')
            ->references('id')->on('types');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
            //
        });
    }
}
