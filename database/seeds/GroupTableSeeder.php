<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * 
     */
   
    public function run()
    {
    DB::table('groups')->delete();
    $json = File::get("database/data/group.json");
    $data =json_decode($json);
    foreach ($data as $groupjson) {
    
        if (!empty($groupjson->name)) {
            $group = App\Group::firstOrCreate(array(
    
                "name"=>$groupjson->name,
                "image_url"=>$groupjson->image_url,
                "description_group"=>$groupjson->description_group,
                "categories_id"=> empty($groupjson->categories_name) ? null : \App\Category::firstOrCreate(array(
                "name"=>$groupjson->categories_name))->id,
                "types_id"=> empty($groupjson->type) ? null : \App\Type::firstOrCreate(array(
                "name"=>$groupjson->type))->id,
             ));
             foreach($groupjson->images_group as $image){
                App\GroupsImages::firstOrCreate(array(
                     "url"=>$image,
                     "groups_id"=>$group->id
                 ));
             }
             foreach($groupjson->members as $memberjson){
                $newmember = App\Member::firstOrCreate(array(
                    
                    "name"=>$memberjson->name,
                    "description_member"=>$memberjson->description_member,
                    "images_member"=>$memberjson->images_member,
                    "groups_id" => $group->id
    
                 ));
                foreach($memberjson->images_list as $url){
                    App\ImagesMembers::firstOrCreate(array(
                        "images_list"=>$url,
                        "members_id"=>$newmember->id
                    ));

                }
            }

        }
    }
    }
}
