<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $json = File::get("database/data/group.json");
        $data =json_decode($json);
        foreach ($data as $obj) {

            if (!empty($obj->categories_name)) {

                App\Category::firstOrCreate(array(
    
                    "name"=>$obj->categories_name,
                    "home_page"=>$obj->home_page,
    
                 ));

            }
        }
    }
}