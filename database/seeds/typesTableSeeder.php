<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class typesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();
        $json = File::get("database/data/group.json");
        $data =json_decode($json);
        foreach ($data as $obj) {

            if (!empty($obj->type)) {

                App\Category::firstOrCreate(array(
    
                    "name"=>$obj->type,  
    
                 ));

            }
        }
    }
}

//     DB::table('types')->delete();
//     DB::table('types')->insert([
//     ['name'=>'Boysband'],
//     ['name'=>'Girlsband'],
// ]);     